<?php

$servername = "mysql-staging-02.cehf1xx1kagh.ap-southeast-1.rds.amazonaws.com";
$username = "common";
$password = "5080d913e21d5dcb18f366fd86b0f5d2";

// Create connection
$conn = new mysqli($servername, $username, $password);
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}
echo "Connected successfully";

$array_tables = [
    "chart_of_account",
    "company_code",
    "component",
    "cost_center",
    "country",
    "currencies",
    "difficulty",
    "district",
    "employee_grade",
    "employee_grade_level",
    "employee_track",
    "grade",
    "grade_groups",
    "legal",
    "package",
    "period",
    "product_center",
    "province",
    "state",
    "subject",
    "ward"
];
$path_module_post = './src/modules/posts/';
$content_list = file_get_contents($path_module_post . "PostList.js");
$content_create = file_get_contents($path_module_post . "PostCreate.js");
$content_edit = file_get_contents($path_module_post . "PostEdit.js");
//$content_table = file_get_contents($path_module_post . "table.js");
//$content_form = file_get_contents($path_module_post . "formData.js");
$content_index = file_get_contents($path_module_post . "index.js");

foreach ($array_tables as $v) {
    $name_folder = str_replace("_", "-", $v);
    $path_folder = './src/modules/' . $name_folder;
    if (!file_exists($path_folder)) {
        mkdir($path_folder, 0777, true);
    }
    $temp = explode("_", $v);
    $name_file = "";
    foreach ($temp as $value) {
        $name_file .= ucfirst($value);
    }
    if (!file_exists($path_folder . "/" . $name_file . "List.js")) {
        $myfile = fopen($path_folder . "/" . $name_file . "List.js", "w") or die("Unable to open file!");
        fwrite($myfile, str_replace("Post", $name_file, $content_list));
        fclose($myfile);
    }
    if (!file_exists($path_folder . "/" . $name_file . "Create.js")) {
        $myfile = fopen($path_folder . "/" . $name_file . "Create.js", "w") or die("Unable to open file!");
        fwrite($myfile, str_replace("Post", $name_file, $content_create));
        fclose($myfile);
    }
    if (!file_exists($path_folder . "/" . $name_file . "Edit.js")) {
        $myfile = fopen($path_folder . "/" . $name_file . "Edit.js", "w") or die("Unable to open file!");
        fwrite($myfile, str_replace("Post", $name_file, $content_edit));
        fclose($myfile);
    }
    if (!file_exists($path_folder . "/" . "index.js")) {
        $myfile = fopen($path_folder . "/" . "index.js", "w") or die("Unable to open file!");
        fwrite($myfile, str_replace("Post", $name_file, $content_index));
        fclose($myfile);
    }

    $content_form = 'import { DATA_TYPE } from "../common/constants";
export const formData = [' . "\n";
    $content_table = 'import { DATA_TYPE } from "../common/constants";
export const tableData = [' . "\n";

    $entry = mysqli_query($conn, "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS
        WHERE TABLE_SCHEMA = 'common' AND TABLE_NAME = '{$v}'");
    while ($row = mysqli_fetch_array($entry)) {
        if ($row["COLUMN_NAME"] !== 'created_at' && $row["COLUMN_NAME"] !== 'updated_at') {
            $content_table .= '{
  data_type: DATA_TYPE.NOTFILL,
  source: "' . $row["COLUMN_NAME"] . '",
},' . "\n";
            if ($row["COLUMN_NAME"] !== 'id') {
                $content_form .= '{
  data_type: DATA_TYPE.NOTFILL,
  source: "' . $row["COLUMN_NAME"] . '",
},' . "\n";
            }
        }
    }
    $content_table .= '];';
    $content_form .= '];';

    $file_table = fopen($path_folder . "/" . "table.js", "w") or die("Unable to open file!");
    fwrite($file_table, $content_table);
    fclose($file_table);
    $file_form = fopen($path_folder . "/" . "formData.js", "w") or die("Unable to open file!");
    fwrite($file_form, $content_form);
    fclose($file_form);
    echo 'import ' . $v . ' from "./modules/' . $name_folder . '" ' . "\n";
}
echo "\n";
foreach ($array_tables as $v) {
    echo '<Resource name="' . $v . '" {...' . $v . '} />' . "\n";
}
mysqli_close($conn);
die();

?>