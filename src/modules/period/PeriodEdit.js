import React from "react";
import {
  Edit,
  SimpleForm,
} from "react-admin";
import FormRender from "../common/components/FormRender";
import {formData} from "./formData";
const PeriodTitle = ({ record }) => {
  return <span>Period {record ? `"${record.title}"` : ""}</span>;
};
export const PeriodEdit = (props) => (
  <Edit title={<PeriodTitle />} undoable={false} {...props}>
    <SimpleForm>
      <FormRender dataSource={formData} />
    </SimpleForm>
  </Edit>
);
