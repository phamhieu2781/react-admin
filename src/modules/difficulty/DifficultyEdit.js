import React from "react";
import {
  Edit,
  SimpleForm,
} from "react-admin";
import FormRender from "../common/components/FormRender";
import {formData} from "./formData";
const DifficultyTitle = ({ record }) => {
  return <span>Difficulty {record ? `"${record.title}"` : ""}</span>;
};
export const DifficultyEdit = (props) => (
  <Edit title={<DifficultyTitle />} undoable={false} {...props}>
    <SimpleForm>
      <FormRender dataSource={formData} />
    </SimpleForm>
  </Edit>
);
