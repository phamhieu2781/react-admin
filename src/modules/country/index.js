import CountryIcon from "@material-ui/icons/Book";
import { CountryList } from "./CountryList";
import { CountryCreate } from "./CountryCreate";
import { CountryEdit } from "./CountryEdit";
export default {
  list: CountryList,
  create: CountryCreate,
  edit: CountryEdit,
  icon: CountryIcon,
};
