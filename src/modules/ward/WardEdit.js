import React from "react";
import {
  Edit,
  SimpleForm,
} from "react-admin";
import FormRender from "../common/components/FormRender";
import {formData} from "./formData";
const WardTitle = ({ record }) => {
  return <span>Ward {record ? `"${record.title}"` : ""}</span>;
};
export const WardEdit = (props) => (
  <Edit title={<WardTitle />} undoable={false} {...props}>
    <SimpleForm>
      <FormRender dataSource={formData} />
    </SimpleForm>
  </Edit>
);
