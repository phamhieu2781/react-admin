import React from "react";
import { Create, SimpleForm } from "react-admin";
import  FormRender  from "../common/components/FormRender";
import { formData } from "./formData";
export const ComponentCreate = (props) => (
  <Create {...props}>
    <SimpleForm>
      <FormRender dataSource={formData} />
    </SimpleForm>
  </Create>
);
