import { DATA_TYPE } from "../common/constants";
export const tableData = [
{
  data_type: DATA_TYPE.NOTFILL,
  source: "id",
},
{
  data_type: DATA_TYPE.NOTFILL,
  source: "parent_id",
},
{
  data_type: DATA_TYPE.NOTFILL,
  source: "code",
},
{
  data_type: DATA_TYPE.NOTFILL,
  source: "name",
},
{
  data_type: DATA_TYPE.NOTFILL,
  source: "name_en",
},
{
  data_type: DATA_TYPE.NOTFILL,
  source: "cf_code",
},
{
  data_type: DATA_TYPE.NOTFILL,
  source: "pl_code",
},
{
  data_type: DATA_TYPE.NOTFILL,
  source: "cf_name",
},
{
  data_type: DATA_TYPE.NOTFILL,
  source: "pl_name",
},
{
  data_type: DATA_TYPE.NOTFILL,
  source: "cf_bh_al1",
},
{
  data_type: DATA_TYPE.NOTFILL,
  source: "cf_ah_bl2",
},
{
  data_type: DATA_TYPE.NOTFILL,
  source: "pl_bd_t1_al31",
},
{
  data_type: DATA_TYPE.NOTFILL,
  source: "pl_bd_t0_al32",
},
{
  data_type: DATA_TYPE.NOTFILL,
  source: "pl_ad_al4",
},
{
  data_type: DATA_TYPE.NOTFILL,
  source: "pl_atp_al5",
},
{
  data_type: DATA_TYPE.NOTFILL,
  source: "state_id",
},
{
  data_type: DATA_TYPE.NOTFILL,
  source: "chart_of_account_id",
},
{
  data_type: DATA_TYPE.NOTFILL,
  source: "description",
},
{
  data_type: DATA_TYPE.NOTFILL,
  source: "created_by",
},
{
  data_type: DATA_TYPE.NOTFILL,
  source: "version",
},
];