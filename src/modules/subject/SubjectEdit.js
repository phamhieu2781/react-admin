import React from "react";
import {
  Edit,
  SimpleForm,
} from "react-admin";
import FormRender from "../common/components/FormRender";
import {formData} from "./formData";
const SubjectTitle = ({ record }) => {
  return <span>Subject {record ? `"${record.title}"` : ""}</span>;
};
export const SubjectEdit = (props) => (
  <Edit title={<SubjectTitle />} undoable={false} {...props}>
    <SimpleForm>
      <FormRender dataSource={formData} />
    </SimpleForm>
  </Edit>
);
