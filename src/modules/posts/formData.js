import { DATA_TYPE } from "../common/constants";
export const formData = [
  {
    data_type: DATA_TYPE.SELECT,
    source: "userId",
    reference: "users",
    optionText: "name",
  },
  {
    data_type: DATA_TYPE.INPUT,
    source: "title",
  },
  {
    data_type: DATA_TYPE.INPUT,
    source: "body",
    multiline: true,
  },
];
