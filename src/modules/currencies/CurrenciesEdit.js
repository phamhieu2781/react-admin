import React from "react";
import {
  Edit,
  SimpleForm,
} from "react-admin";
import FormRender from "../common/components/FormRender";
import {formData} from "./formData";
const CurrenciesTitle = ({ record }) => {
  return <span>Currencies {record ? `"${record.title}"` : ""}</span>;
};
export const CurrenciesEdit = (props) => (
  <Edit title={<CurrenciesTitle />} undoable={false} {...props}>
    <SimpleForm>
      <FormRender dataSource={formData} />
    </SimpleForm>
  </Edit>
);
