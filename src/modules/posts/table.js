import { DATA_TYPE } from "../common/constants";
export const tableData = [
  {
    data_type: DATA_TYPE.INPUT,
    source: "id",
  },
  {
    data_type: DATA_TYPE.SELECT,
    source: "userId",
    reference: "users",
    optionText: "name",
  },
  {
    data_type: DATA_TYPE.INPUT,
    source: "title",
  },
];