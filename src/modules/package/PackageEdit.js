import React from "react";
import {
  Edit,
  SimpleForm,
} from "react-admin";
import FormRender from "../common/components/FormRender";
import {formData} from "./formData";
const PackageTitle = ({ record }) => {
  return <span>Package {record ? `"${record.title}"` : ""}</span>;
};
export const PackageEdit = (props) => (
  <Edit title={<PackageTitle />} undoable={false} {...props}>
    <SimpleForm>
      <FormRender dataSource={formData} />
    </SimpleForm>
  </Edit>
);
