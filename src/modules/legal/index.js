import LegalIcon from "@material-ui/icons/Book";
import { LegalList } from "./LegalList";
import { LegalCreate } from "./LegalCreate";
import { LegalEdit } from "./LegalEdit";
export default {
  list: LegalList,
  create: LegalCreate,
  edit: LegalEdit,
  icon: LegalIcon,
};
