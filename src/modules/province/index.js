import ProvinceIcon from "@material-ui/icons/Book";
import { ProvinceList } from "./ProvinceList";
import { ProvinceCreate } from "./ProvinceCreate";
import { ProvinceEdit } from "./ProvinceEdit";
export default {
  list: ProvinceList,
  create: ProvinceCreate,
  edit: ProvinceEdit,
  icon: ProvinceIcon,
};
