import { DATA_TYPE } from "../common/constants";
export const formData = [
{
  data_type: DATA_TYPE.NOTFILL,
  source: "code",
},
{
  data_type: DATA_TYPE.NOTFILL,
  source: "name",
},
{
  data_type: DATA_TYPE.NOTFILL,
  source: "type",
},
{
  data_type: DATA_TYPE.NOTFILL,
  source: "district_id",
},
{
  data_type: DATA_TYPE.NOTFILL,
  source: "is_deleted",
},
{
  data_type: DATA_TYPE.NOTFILL,
  source: "state_id",
},
{
  data_type: DATA_TYPE.NOTFILL,
  source: "version",
},
];