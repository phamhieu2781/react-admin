import { DATA_TYPE } from "../common/constants";
export const formData = [
  {
    data_type: DATA_TYPE.INPUT,
    source: "name",
  },
  {
    data_type: DATA_TYPE.INPUT,
    source: "email",
  },
  {
    data_type: DATA_TYPE.INPUT,
    source: "phone",
  },
  {
    data_type: DATA_TYPE.INPUT,
    source: "website",
  },
  {
    data_type: DATA_TYPE.DATEPICKER,
    source: "created_at",
  },
  {
    data_type: DATA_TYPE.SELECT,
    source: "companyId",
    reference: "companies",
    optionText: "name",
  },
];
