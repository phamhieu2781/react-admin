import React from "react";
import {
  Edit,
  SimpleForm,
} from "react-admin";
import FormRender from "../common/components/FormRender";
import {formData} from "./formData";
const ComponentTitle = ({ record }) => {
  return <span>Component {record ? `"${record.title}"` : ""}</span>;
};
export const ComponentEdit = (props) => (
  <Edit title={<ComponentTitle />} undoable={false} {...props}>
    <SimpleForm>
      <FormRender dataSource={formData} />
    </SimpleForm>
  </Edit>
);
