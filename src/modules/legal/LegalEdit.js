import React from "react";
import {
  Edit,
  SimpleForm,
} from "react-admin";
import FormRender from "../common/components/FormRender";
import {formData} from "./formData";
const LegalTitle = ({ record }) => {
  return <span>Legal {record ? `"${record.title}"` : ""}</span>;
};
export const LegalEdit = (props) => (
  <Edit title={<LegalTitle />} undoable={false} {...props}>
    <SimpleForm>
      <FormRender dataSource={formData} />
    </SimpleForm>
  </Edit>
);
