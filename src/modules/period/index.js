import PeriodIcon from "@material-ui/icons/Book";
import { PeriodList } from "./PeriodList";
import { PeriodCreate } from "./PeriodCreate";
import { PeriodEdit } from "./PeriodEdit";
export default {
  list: PeriodList,
  create: PeriodCreate,
  edit: PeriodEdit,
  icon: PeriodIcon,
};
