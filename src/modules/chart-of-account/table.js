import { DATA_TYPE } from "../common/constants";
export const tableData = [
{
  data_type: DATA_TYPE.NOTFILL,
  source: "id",
},
{
  data_type: DATA_TYPE.NOTFILL,
  source: "parent_id",
},
{
  data_type: DATA_TYPE.NOTFILL,
  source: "code",
},
{
  data_type: DATA_TYPE.NOTFILL,
  source: "name",
},
{
  data_type: DATA_TYPE.NOTFILL,
  source: "name_en",
},
{
  data_type: DATA_TYPE.NOTFILL,
  source: "state_id",
},
{
  data_type: DATA_TYPE.NOTFILL,
  source: "description",
},
{
  data_type: DATA_TYPE.NOTFILL,
  source: "created_by",
},
{
  data_type: DATA_TYPE.NOTFILL,
  source: "account_property_id",
},
{
  data_type: DATA_TYPE.NOTFILL,
  source: "version",
},
];