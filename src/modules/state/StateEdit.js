import React from "react";
import {
  Edit,
  SimpleForm,
} from "react-admin";
import FormRender from "../common/components/FormRender";
import {formData} from "./formData";
const StateTitle = ({ record }) => {
  return <span>State {record ? `"${record.title}"` : ""}</span>;
};
export const StateEdit = (props) => (
  <Edit title={<StateTitle />} undoable={false} {...props}>
    <SimpleForm>
      <FormRender dataSource={formData} />
    </SimpleForm>
  </Edit>
);
