import DistrictIcon from "@material-ui/icons/Book";
import { DistrictList } from "./DistrictList";
import { DistrictCreate } from "./DistrictCreate";
import { DistrictEdit } from "./DistrictEdit";
export default {
  list: DistrictList,
  create: DistrictCreate,
  edit: DistrictEdit,
  icon: DistrictIcon,
};
