import React from "react";
import { DATA_TYPE } from "../constants";
import { ReferenceInput, SelectInput, TextInput, DateInput } from "react-admin";
const FormRenderItem = ({
  data_type,
  source,
  reference = "",
  optionText = "",
  ...props
}) => {
  let render = null;
  switch (data_type) {
    case DATA_TYPE.INPUT: {
      render = <TextInput source={source} {...props} />;
      break;
    }
    case DATA_TYPE.SELECT: {
      render = (
        <ReferenceInput source={source} reference={reference}>
          <SelectInput optionText={optionText} />
        </ReferenceInput>
      );
      break;
    }
    case DATA_TYPE.DATEPICKER: {
      render = <DateInput source={source} />;
      break;
    }
  }
  return <div className="custom-field-render">{render}</div>;
};

const FormRender = ({ dataSource }) => {
  return dataSource.map(({ source, ...props }, k) => (
    <FormRenderItem key={k} source={source} {...props} />
  ));
};
export default React.memo(FormRender);