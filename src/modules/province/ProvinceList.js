import React from "react";
import {
  List,
  ReferenceInput,
  SelectInput,
  TextInput,
  Filter,
} from "react-admin";
import TableRender from "../common/components/TableRender";
import {tableData} from "./table";

const ProvinceFilter = (props) => (
  <Filter {...props}>
    <TextInput label="Search" source="q" alwaysOn />
    <ReferenceInput label="User" source="userId" reference="users" allowEmpty>
      <SelectInput optionText="name" />
    </ReferenceInput>
  </Filter>
);

export const ProvinceList = (props) => (
  <List filters={<ProvinceFilter />} {...props}>
      <TableRender dataSource={tableData} />
  </List>
);
