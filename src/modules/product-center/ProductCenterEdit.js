import React from "react";
import {
  Edit,
  SimpleForm,
} from "react-admin";
import FormRender from "../common/components/FormRender";
import {formData} from "./formData";
const ProductCenterTitle = ({ record }) => {
  return <span>ProductCenter {record ? `"${record.title}"` : ""}</span>;
};
export const ProductCenterEdit = (props) => (
  <Edit title={<ProductCenterTitle />} undoable={false} {...props}>
    <SimpleForm>
      <FormRender dataSource={formData} />
    </SimpleForm>
  </Edit>
);
