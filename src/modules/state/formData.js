import { DATA_TYPE } from "../common/constants";
export const formData = [
{
  data_type: DATA_TYPE.NOTFILL,
  source: "code",
},
{
  data_type: DATA_TYPE.NOTFILL,
  source: "name",
},
{
  data_type: DATA_TYPE.NOTFILL,
  source: "name_en",
},
{
  data_type: DATA_TYPE.NOTFILL,
  source: "description",
},
{
  data_type: DATA_TYPE.NOTFILL,
  source: "created_by",
},
{
  data_type: DATA_TYPE.NOTFILL,
  source: "version",
},
];