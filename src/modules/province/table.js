import { DATA_TYPE } from "../common/constants";
export const tableData = [
{
  data_type: DATA_TYPE.NOTFILL,
  source: "id",
},
{
  data_type: DATA_TYPE.NOTFILL,
  source: "code",
},
{
  data_type: DATA_TYPE.NOTFILL,
  source: "name",
},
{
  data_type: DATA_TYPE.NOTFILL,
  source: "type",
},
{
  data_type: DATA_TYPE.NOTFILL,
  source: "telephone_code",
},
{
  data_type: DATA_TYPE.NOTFILL,
  source: "zip_code",
},
{
  data_type: DATA_TYPE.NOTFILL,
  source: "country_id",
},
{
  data_type: DATA_TYPE.NOTFILL,
  source: "is_deleted",
},
{
  data_type: DATA_TYPE.NOTFILL,
  source: "state_id",
},
{
  data_type: DATA_TYPE.NOTFILL,
  source: "version",
},
];