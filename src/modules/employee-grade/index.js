import EmployeeGradeIcon from "@material-ui/icons/Book";
import { EmployeeGradeList } from "./EmployeeGradeList";
import { EmployeeGradeCreate } from "./EmployeeGradeCreate";
import { EmployeeGradeEdit } from "./EmployeeGradeEdit";
export default {
  list: EmployeeGradeList,
  create: EmployeeGradeCreate,
  edit: EmployeeGradeEdit,
  icon: EmployeeGradeIcon,
};
