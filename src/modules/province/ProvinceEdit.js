import React from "react";
import {
  Edit,
  SimpleForm,
} from "react-admin";
import FormRender from "../common/components/FormRender";
import {formData} from "./formData";
const ProvinceTitle = ({ record }) => {
  return <span>Province {record ? `"${record.title}"` : ""}</span>;
};
export const ProvinceEdit = (props) => (
  <Edit title={<ProvinceTitle />} undoable={false} {...props}>
    <SimpleForm>
      <FormRender dataSource={formData} />
    </SimpleForm>
  </Edit>
);
