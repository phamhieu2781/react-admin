import React from "react";
import {
  Edit,
  SimpleForm,
} from "react-admin";
import FormRender from "../common/components/FormRender";
import {formData} from "./formData";
const CountryTitle = ({ record }) => {
  return <span>Country {record ? `"${record.title}"` : ""}</span>;
};
export const CountryEdit = (props) => (
  <Edit title={<CountryTitle />} undoable={false} {...props}>
    <SimpleForm>
      <FormRender dataSource={formData} />
    </SimpleForm>
  </Edit>
);
