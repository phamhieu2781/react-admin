import React from "react";
import {
  Edit,
  SimpleForm,
} from "react-admin";
import FormRender from "../common/components/FormRender";
import {formData} from "./formData";
const CostCenterTitle = ({ record }) => {
  return <span>CostCenter {record ? `"${record.title}"` : ""}</span>;
};
export const CostCenterEdit = (props) => (
  <Edit title={<CostCenterTitle />} undoable={false} {...props}>
    <SimpleForm>
      <FormRender dataSource={formData} />
    </SimpleForm>
  </Edit>
);
