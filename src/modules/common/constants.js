export const DATA_TYPE = {
    INPUT: "Input",
    TEXTAREA: "Textarea",
    SELECT: "Select",
    DATEPICKER: "DatePicker",
    URL: "Url",
    NOTFILL: "Input",
}