import React from "react";
import {
  Edit,
  SimpleForm,
} from "react-admin";
import FormRender from "../common/components/FormRender";
import {formData} from "./formData";
const EmployeeGradeLevelTitle = ({ record }) => {
  return <span>EmployeeGradeLevel {record ? `"${record.title}"` : ""}</span>;
};
export const EmployeeGradeLevelEdit = (props) => (
  <Edit title={<EmployeeGradeLevelTitle />} undoable={false} {...props}>
    <SimpleForm>
      <FormRender dataSource={formData} />
    </SimpleForm>
  </Edit>
);
