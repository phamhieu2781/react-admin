import React from "react";
import {
  Edit,
  SimpleForm,
} from "react-admin";
import FormRender from "../common/components/FormRender";
import {formData} from "./formData";
const UserTitle = ({ record }) => {
  return <span>User {record ? `"${record.title}"` : ""}</span>;
};
export const UserEdit = (props) => (
  <Edit title={<UserTitle />} undoable={false} {...props}>
    <SimpleForm>
      <FormRender dataSource={formData} />
    </SimpleForm>
  </Edit>
);
