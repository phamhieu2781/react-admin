import PackageIcon from "@material-ui/icons/Book";
import { PackageList } from "./PackageList";
import { PackageCreate } from "./PackageCreate";
import { PackageEdit } from "./PackageEdit";
export default {
  list: PackageList,
  create: PackageCreate,
  edit: PackageEdit,
  icon: PackageIcon,
};
