import ChartOfAccountIcon from "@material-ui/icons/Book";
import { ChartOfAccountList } from "./ChartOfAccountList";
import { ChartOfAccountCreate } from "./ChartOfAccountCreate";
import { ChartOfAccountEdit } from "./ChartOfAccountEdit";
export default {
  list: ChartOfAccountList,
  create: ChartOfAccountCreate,
  edit: ChartOfAccountEdit,
  icon: ChartOfAccountIcon,
};
