import ComponentIcon from "@material-ui/icons/Book";
import { ComponentList } from "./ComponentList";
import { ComponentCreate } from "./ComponentCreate";
import { ComponentEdit } from "./ComponentEdit";
export default {
  list: ComponentList,
  create: ComponentCreate,
  edit: ComponentEdit,
  icon: ComponentIcon,
};
