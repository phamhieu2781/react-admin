import React from "react";
import { DATA_TYPE } from "../constants";
import { TextField, ReferenceField, Datagrid, EditButton } from "react-admin";
import MyUrlField from "./MyUrlField";
const TableRenderItem = ({
  data_type,
  source,
  reference = "",
  optionText = "",
  ...props
}) => {
  let render = null;
  switch (data_type) {
    case DATA_TYPE.INPUT: {
      render = <TextField source={source} {...props} />;
      break;
    }
    case DATA_TYPE.URL: {
        render = <MyUrlField source={source} {...props} />;
        break;
      }
    case DATA_TYPE.SELECT: {
      render = (
        <ReferenceField source={source} reference={reference} {...props}>
          <TextField source={optionText} />
        </ReferenceField>
      );
      break;
    }
    default: {
      render = <TextField source={source} {...props} />;
    }
  }
  return <>{render}</>;
};

const TableRender = ({ dataSource, enableEdit = true }) => {
  return (
    <Datagrid>
      {dataSource.map(({ data_type, source, ...props }, k) => (
        <TableRenderItem
          key={k}
          data_type={data_type}
          source={source}
          {...props}
        />
      ))}
      {enableEdit && <EditButton />}
    </Datagrid>
  );
};
export default React.memo(TableRender);
