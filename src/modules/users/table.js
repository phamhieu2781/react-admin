import { DATA_TYPE } from "../common/constants";
export const tableData = [
  {
    data_type: DATA_TYPE.INPUT,
    source: "id",
  },
  {
    data_type: DATA_TYPE.INPUT,
    source: "name",
  },
  {
    data_type: DATA_TYPE.INPUT,
    source: "email",
  },
  {
    data_type: DATA_TYPE.INPUT,
    source: "phone",
  },
  {
    data_type: DATA_TYPE.URL,
    source: "website",
  },
  {
    data_type: DATA_TYPE.INPUT,
    source: "company.name",
  },
];