import ProductCenterIcon from "@material-ui/icons/Book";
import { ProductCenterList } from "./ProductCenterList";
import { ProductCenterCreate } from "./ProductCenterCreate";
import { ProductCenterEdit } from "./ProductCenterEdit";
export default {
  list: ProductCenterList,
  create: ProductCenterCreate,
  edit: ProductCenterEdit,
  icon: ProductCenterIcon,
};
