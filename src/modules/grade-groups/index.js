import GradeGroupsIcon from "@material-ui/icons/Book";
import { GradeGroupsList } from "./GradeGroupsList";
import { GradeGroupsCreate } from "./GradeGroupsCreate";
import { GradeGroupsEdit } from "./GradeGroupsEdit";
export default {
  list: GradeGroupsList,
  create: GradeGroupsCreate,
  edit: GradeGroupsEdit,
  icon: GradeGroupsIcon,
};
