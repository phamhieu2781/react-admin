import WardIcon from "@material-ui/icons/Book";
import { WardList } from "./WardList";
import { WardCreate } from "./WardCreate";
import { WardEdit } from "./WardEdit";
export default {
  list: WardList,
  create: WardCreate,
  edit: WardEdit,
  icon: WardIcon,
};
