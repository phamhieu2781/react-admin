import { DATA_TYPE } from "../common/constants";
export const formData = [
{
  data_type: DATA_TYPE.NOTFILL,
  source: "parent_id",
},
{
  data_type: DATA_TYPE.NOTFILL,
  source: "code",
},
{
  data_type: DATA_TYPE.NOTFILL,
  source: "name",
},
{
  data_type: DATA_TYPE.NOTFILL,
  source: "name_en",
},
{
  data_type: DATA_TYPE.NOTFILL,
  source: "description",
},
{
  data_type: DATA_TYPE.NOTFILL,
  source: "company_address",
},
{
  data_type: DATA_TYPE.NOTFILL,
  source: "issue_date",
},
{
  data_type: DATA_TYPE.NOTFILL,
  source: "legal_representative",
},
{
  data_type: DATA_TYPE.NOTFILL,
  source: "company_tax_number",
},
{
  data_type: DATA_TYPE.NOTFILL,
  source: "authorized_representatives_of_shareholder",
},
{
  data_type: DATA_TYPE.NOTFILL,
  source: "state_id",
},
{
  data_type: DATA_TYPE.NOTFILL,
  source: "created_by",
},
{
  data_type: DATA_TYPE.NOTFILL,
  source: "version",
},
];