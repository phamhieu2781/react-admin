import StateIcon from "@material-ui/icons/Book";
import { StateList } from "./StateList";
import { StateCreate } from "./StateCreate";
import { StateEdit } from "./StateEdit";
export default {
  list: StateList,
  create: StateCreate,
  edit: StateEdit,
  icon: StateIcon,
};
