import React from "react";
import {
  Edit,
  SimpleForm,
} from "react-admin";
import FormRender from "../common/components/FormRender";
import {formData} from "./formData";
const GradeTitle = ({ record }) => {
  return <span>Grade {record ? `"${record.title}"` : ""}</span>;
};
export const GradeEdit = (props) => (
  <Edit title={<GradeTitle />} undoable={false} {...props}>
    <SimpleForm>
      <FormRender dataSource={formData} />
    </SimpleForm>
  </Edit>
);
