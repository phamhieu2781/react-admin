import { DATA_TYPE } from "../common/constants";
export const formData = [
{
  data_type: DATA_TYPE.NOTFILL,
  source: "name",
},
{
  data_type: DATA_TYPE.NOTFILL,
  source: "code",
},
{
  data_type: DATA_TYPE.NOTFILL,
  source: "symbol",
},
{
  data_type: DATA_TYPE.NOTFILL,
  source: "description",
},
{
  data_type: DATA_TYPE.NOTFILL,
  source: "state_id",
},
{
  data_type: DATA_TYPE.NOTFILL,
  source: "version",
},
];