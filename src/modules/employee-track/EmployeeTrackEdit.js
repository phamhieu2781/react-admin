import React from "react";
import {
  Edit,
  SimpleForm,
} from "react-admin";
import FormRender from "../common/components/FormRender";
import {formData} from "./formData";
const EmployeeTrackTitle = ({ record }) => {
  return <span>EmployeeTrack {record ? `"${record.title}"` : ""}</span>;
};
export const EmployeeTrackEdit = (props) => (
  <Edit title={<EmployeeTrackTitle />} undoable={false} {...props}>
    <SimpleForm>
      <FormRender dataSource={formData} />
    </SimpleForm>
  </Edit>
);
