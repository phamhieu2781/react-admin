import EmployeeGradeLevelIcon from "@material-ui/icons/Book";
import { EmployeeGradeLevelList } from "./EmployeeGradeLevelList";
import { EmployeeGradeLevelCreate } from "./EmployeeGradeLevelCreate";
import { EmployeeGradeLevelEdit } from "./EmployeeGradeLevelEdit";
export default {
  list: EmployeeGradeLevelList,
  create: EmployeeGradeLevelCreate,
  edit: EmployeeGradeLevelEdit,
  icon: EmployeeGradeLevelIcon,
};
