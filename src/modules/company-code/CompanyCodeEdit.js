import React from "react";
import {
  Edit,
  SimpleForm,
} from "react-admin";
import FormRender from "../common/components/FormRender";
import {formData} from "./formData";
const CompanyCodeTitle = ({ record }) => {
  return <span>CompanyCode {record ? `"${record.title}"` : ""}</span>;
};
export const CompanyCodeEdit = (props) => (
  <Edit title={<CompanyCodeTitle />} undoable={false} {...props}>
    <SimpleForm>
      <FormRender dataSource={formData} />
    </SimpleForm>
  </Edit>
);
