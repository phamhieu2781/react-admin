import React from "react";
import {
  Edit,
  SimpleForm,
} from "react-admin";
import FormRender from "../common/components/FormRender";
import {formData} from "./formData";
const EmployeeGradeTitle = ({ record }) => {
  return <span>EmployeeGrade {record ? `"${record.title}"` : ""}</span>;
};
export const EmployeeGradeEdit = (props) => (
  <Edit title={<EmployeeGradeTitle />} undoable={false} {...props}>
    <SimpleForm>
      <FormRender dataSource={formData} />
    </SimpleForm>
  </Edit>
);
