import CurrenciesIcon from "@material-ui/icons/Book";
import { CurrenciesList } from "./CurrenciesList";
import { CurrenciesCreate } from "./CurrenciesCreate";
import { CurrenciesEdit } from "./CurrenciesEdit";
export default {
  list: CurrenciesList,
  create: CurrenciesCreate,
  edit: CurrenciesEdit,
  icon: CurrenciesIcon,
};
