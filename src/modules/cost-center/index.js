import CostCenterIcon from "@material-ui/icons/Book";
import { CostCenterList } from "./CostCenterList";
import { CostCenterCreate } from "./CostCenterCreate";
import { CostCenterEdit } from "./CostCenterEdit";
export default {
  list: CostCenterList,
  create: CostCenterCreate,
  edit: CostCenterEdit,
  icon: CostCenterIcon,
};
