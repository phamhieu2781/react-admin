import React from "react";
import {
  Edit,
  SimpleForm,
} from "react-admin";
import FormRender from "../common/components/FormRender";
import {formData} from "./formData";
const GradeGroupsTitle = ({ record }) => {
  return <span>GradeGroups {record ? `"${record.title}"` : ""}</span>;
};
export const GradeGroupsEdit = (props) => (
  <Edit title={<GradeGroupsTitle />} undoable={false} {...props}>
    <SimpleForm>
      <FormRender dataSource={formData} />
    </SimpleForm>
  </Edit>
);
