import CompanyCodeIcon from "@material-ui/icons/Book";
import { CompanyCodeList } from "./CompanyCodeList";
import { CompanyCodeCreate } from "./CompanyCodeCreate";
import { CompanyCodeEdit } from "./CompanyCodeEdit";
export default {
  list: CompanyCodeList,
  create: CompanyCodeCreate,
  edit: CompanyCodeEdit,
  icon: CompanyCodeIcon,
};
