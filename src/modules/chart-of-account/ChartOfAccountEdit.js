import React from "react";
import {
  Edit,
  SimpleForm,
} from "react-admin";
import FormRender from "../common/components/FormRender";
import {formData} from "./formData";
const ChartOfAccountTitle = ({ record }) => {
  return <span>ChartOfAccount {record ? `"${record.title}"` : ""}</span>;
};
export const ChartOfAccountEdit = (props) => (
  <Edit title={<ChartOfAccountTitle />} undoable={false} {...props}>
    <SimpleForm>
      <FormRender dataSource={formData} />
    </SimpleForm>
  </Edit>
);
