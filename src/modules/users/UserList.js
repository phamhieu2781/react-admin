import React from "react";
import {
  List,
  ReferenceInput,
  SelectInput,
  TextInput,
  Filter,
} from "react-admin";
import TableRender from "../common/components/TableRender";
import {tableData} from "./table";

const UserFilter = (props) => (
  <Filter {...props}>
    <TextInput label="Search" source="q" alwaysOn />
    <ReferenceInput label="User" source="userId" reference="users" allowEmpty>
      <SelectInput optionText="name" />
    </ReferenceInput>
  </Filter>
);

export const UserList = (props) => (
  <List filters={<UserFilter />} {...props}>
      <TableRender dataSource={tableData} />
  </List>
);
