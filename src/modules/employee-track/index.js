import EmployeeTrackIcon from "@material-ui/icons/Book";
import { EmployeeTrackList } from "./EmployeeTrackList";
import { EmployeeTrackCreate } from "./EmployeeTrackCreate";
import { EmployeeTrackEdit } from "./EmployeeTrackEdit";
export default {
  list: EmployeeTrackList,
  create: EmployeeTrackCreate,
  edit: EmployeeTrackEdit,
  icon: EmployeeTrackIcon,
};
