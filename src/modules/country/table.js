import { DATA_TYPE } from "../common/constants";
export const tableData = [
{
  data_type: DATA_TYPE.NOTFILL,
  source: "id",
},
{
  data_type: DATA_TYPE.NOTFILL,
  source: "country_code",
},
{
  data_type: DATA_TYPE.NOTFILL,
  source: "country_name",
},
{
  data_type: DATA_TYPE.NOTFILL,
  source: "formal_name",
},
{
  data_type: DATA_TYPE.NOTFILL,
  source: "capital",
},
{
  data_type: DATA_TYPE.NOTFILL,
  source: "currency_code",
},
{
  data_type: DATA_TYPE.NOTFILL,
  source: "currency_name",
},
{
  data_type: DATA_TYPE.NOTFILL,
  source: "telephone_code",
},
{
  data_type: DATA_TYPE.NOTFILL,
  source: "internet_country_code",
},
{
  data_type: DATA_TYPE.NOTFILL,
  source: "is_deleted",
},
{
  data_type: DATA_TYPE.NOTFILL,
  source: "state_id",
},
{
  data_type: DATA_TYPE.NOTFILL,
  source: "version",
},
];