import DifficultyIcon from "@material-ui/icons/Book";
import { DifficultyList } from "./DifficultyList";
import { DifficultyCreate } from "./DifficultyCreate";
import { DifficultyEdit } from "./DifficultyEdit";
export default {
  list: DifficultyList,
  create: DifficultyCreate,
  edit: DifficultyEdit,
  icon: DifficultyIcon,
};
