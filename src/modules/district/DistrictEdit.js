import React from "react";
import {
  Edit,
  SimpleForm,
} from "react-admin";
import FormRender from "../common/components/FormRender";
import {formData} from "./formData";
const DistrictTitle = ({ record }) => {
  return <span>District {record ? `"${record.title}"` : ""}</span>;
};
export const DistrictEdit = (props) => (
  <Edit title={<DistrictTitle />} undoable={false} {...props}>
    <SimpleForm>
      <FormRender dataSource={formData} />
    </SimpleForm>
  </Edit>
);
