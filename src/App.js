import React from "react";
import { Admin, Resource } from "react-admin";
import posts from "./modules/posts";
import users from "./modules/users";
import Dashboard from "./modules/dashboard/Dashboard";

import chart_of_account from "./modules/chart-of-account"
import company_code from "./modules/company-code"
import component from "./modules/component"
import cost_center from "./modules/cost-center"
import country from "./modules/country"
import currencies from "./modules/currencies"
import difficulty from "./modules/difficulty"
import district from "./modules/district"
import employee_grade from "./modules/employee-grade"
import employee_grade_level from "./modules/employee-grade-level"
import employee_track from "./modules/employee-track"
import grade from "./modules/grade"
import grade_groups from "./modules/grade-groups"
import legal from "./modules/legal"
import packages from "./modules/package"
import period from "./modules/period"
import product_center from "./modules/product-center"
import province from "./modules/province"
import state from "./modules/state"
import subject from "./modules/subject"
import ward from "./modules/ward"


import authProvider from "./authProvider";
import jsonServerProvider from "ra-data-json-server";
const API_URL = process.env.REACT_APP_API_URL;
const dataProvider = jsonServerProvider(API_URL);

const App = () => (
  <Admin
    dashboard={Dashboard}
    authProvider={authProvider}
    dataProvider={dataProvider}
  >
    <Resource name="posts" {...posts} />
    {/* <Resource name="users" list={UserList} icon={UserIcon} /> */}
    <Resource name="users" {...users} />

    <Resource name="chart_of_account" {...chart_of_account} />
    <Resource name="company_code" {...company_code} />
    <Resource name="component" {...component} />
    <Resource name="cost_center" {...cost_center} />
    <Resource name="country" {...country} />
    <Resource name="currencies" {...currencies} />
    <Resource name="difficulty" {...difficulty} />
    <Resource name="district" {...district} />
    <Resource name="employee_grade" {...employee_grade} />
    <Resource name="employee_grade_level" {...employee_grade_level} />
    <Resource name="employee_track" {...employee_track} />
    <Resource name="grade" {...grade} />
    <Resource name="grade_groups" {...grade_groups} />
    <Resource name="legal" {...legal} />
    <Resource name="package" {...packages} />
    <Resource name="period" {...period} />
    <Resource name="product_center" {...product_center} />
    <Resource name="province" {...province} />
    <Resource name="state" {...state} />
    <Resource name="subject" {...subject} />
    <Resource name="ward" {...ward} />
  </Admin>
);

export default App;
